﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Target : MonoBehaviour
{
    [SerializeField] private float timeToExplode = 2.0f;
    private bool isExploding = false;

    public void Explode()
    {
        if (!isExploding)
        {
            StartCoroutine(Exploding());
        }
    }

    private IEnumerator Exploding()
    {
        isExploding = true;
        float timer = 0.0f;
        Vector3 originalScale = transform.localScale;
        while (timer <= timeToExplode)
        {
            transform.localScale = originalScale - Vector3.one * (timer / timeToExplode);
            timer += Time.deltaTime;
            yield return null;
        }

        GameManager.Instance.NbrTargets--;
        if (GameManager.Instance.NbrTargets <= 0)
        {
            GameManager.Instance.Torch.MainText.text =
                "Merci de votre participation! Le jeu va recommencer dans 5 secondes.";
            yield return new WaitForSeconds(5);
            GameManager.Instance.LoadLevel(SceneManager.GetActiveScene().name);
        }

        Destroy(this.gameObject);
        yield return null;
    }
}