﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{ 
    [SerializeField] public bool allumerLumiere = true;
    [SerializeField, Range(30.0f, 80.0f)] private float angleSpotLumiere = 60.0f;
    [SerializeField] private bool autoriserVacillementLumiere = false;
    [SerializeField, Range(0, 6.0f)] private float minIntensiteLumiere = 0f;
    [SerializeField, Range(0, 6.0f)] private float maxIntensiteLumiere = 1f;
    [SerializeField, Range(1, 50)] private int adouciVacillement = 10;
    [SerializeField] private Color couleurLumiere = Color.white;
    private int nbrTargets = 0;
    private Torch torch;

    public Torch Torch => torch;

    public int NbrTargets
    {
        get => nbrTargets;
        set => nbrTargets = value;
    }
    
    public static GameManager Instance { get; private set; }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoadingScene;
    }

    private void Update()
    {
        torch.enableLight = allumerLumiere;
        torch.spotAngle = angleSpotLumiere;
        torch.enableFlicker = autoriserVacillementLumiere;
        torch.minIntensityFlicker = minIntensiteLumiere;
        torch.maxIntensityFlicker = maxIntensiteLumiere;
        torch.smoothingFlicker = adouciVacillement;
        torch.myLightColor = couleurLumiere;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoadingScene;
    }

    //this function is activated every time a scene is loaded
    private void OnLevelFinishedLoadingScene(Scene scene, LoadSceneMode mode)
    {
        Setup();
    }

    private void Setup()
    {
        nbrTargets = FindObjectsOfType<Target>().Length;
        torch = FindObjectOfType<Torch>();
        allumerLumiere = true;
        angleSpotLumiere = 60;
        autoriserVacillementLumiere = false;
        minIntensiteLumiere = 0;
        maxIntensiteLumiere = 3;
        adouciVacillement = 35;
        //alternative way to get elements. cons : if there is no element with such tag it creates an error
        //_player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        //_player = FindObjectOfType<PlayerController>();
    }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        Setup();
    }

    public void LoadLevel(string nameLevel)
    {
        SceneManager.LoadScene(nameLevel);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
    }
}