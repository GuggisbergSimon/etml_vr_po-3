﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Torch : MonoBehaviour
{
    [SerializeField] public bool enableLight = true;
    [SerializeField, Range(30.0f, 80.0f)] public float spotAngle = 60.0f;
    [SerializeField] public bool enableFlicker = false;
    [SerializeField, Range(0, 6.0f)] public float minIntensityFlicker = 0f;
    [SerializeField, Range(0, 6.0f)] public float maxIntensityFlicker = 1f;
    [SerializeField, Range(1, 50)] public int smoothingFlicker = 10;
    [SerializeField] private TextMeshProUGUI mainText = null;
    [SerializeField] public Color myLightColor = Color.white;
    private float timeToQuit = 5.0f;
    private bool hasSeenLightText = false;
    private Coroutine quitCoroutine;
    private LineRenderer myLineDebug;
    private Light myLight;
    private Queue<float> smoothQueue;
    private float lastSum = 0;
    private float initIntensity;

    public TextMeshProUGUI MainText => mainText;

    private void Start()
    {
        myLineDebug = GetComponent<LineRenderer>();
        myLight = GetComponent<Light>();
        initIntensity = myLight.intensity;
        smoothQueue = new Queue<float>(smoothingFlicker);
    }

    IEnumerator Quit()
    {
        yield return new WaitForSeconds(timeToQuit);
        GameManager.Instance.LoadLevel(SceneManager.GetActiveScene().name);
    }

    private void Update()
    {
        //Check for reloadScene
        if (Input.GetButtonDown("Oculus_CrossPlatform_Button2"))
        {
            quitCoroutine = StartCoroutine(Quit());
        }
        else if (Input.GetButtonUp("Oculus_CrossPlatform_Button2"))
        {
            if (quitCoroutine != null)
            {
                StopCoroutine(quitCoroutine);
            }
        }

        //Raycast check
        RaycastHit hit;
        if (Physics.Raycast(this.transform.position, this.transform.forward, out hit))
        {
            myLineDebug.SetPosition(0, transform.position);
            myLineDebug.SetPosition(1, hit.point);

            if ((Input.GetAxis("Oculus_GearVR_RIndexTrigger") > 0 ||
                 Input.GetAxis("Oculus_GearVR_RHandTrigger") > 0) &&
                hit.transform.gameObject.CompareTag("Target"))
            {
                mainText.text = "";
                hit.transform.GetComponentInParent<Target>().Explode();
            }
        }

        //Update light values based on inspector
        myLight.spotAngle = spotAngle;
        myLight.color = myLightColor;
        myLight.enabled = enableLight;

        //On-Off check
        if (Input.GetButtonDown("Oculus_CrossPlatform_Button2"))
        {
            mainText.text = "";
            myLight.enabled = !myLight.enabled;
            enableLight = myLight.enabled;
            GameManager.Instance.allumerLumiere = myLight.enabled;
            myLight.color = Color.white;
        }

        if (!myLight.enabled && !hasSeenLightText)
        {
            mainText.text = "Appuyez sur un bouton près de votre pouce pour rallumer votre lampe.";
            hasSeenLightText = true;
        }

        //Flicker code
        if (enableFlicker)
        {
            while (smoothQueue.Count >= smoothingFlicker)
            {
                lastSum -= smoothQueue.Dequeue();
            }

            float newVal = Random.Range(minIntensityFlicker, maxIntensityFlicker);
            smoothQueue.Enqueue(newVal);
            lastSum += newVal;
            myLight.intensity = lastSum / smoothQueue.Count;
        }
        else
        {
            myLight.intensity = initIntensity;
        }
    }
}